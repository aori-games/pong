import java.awt.event.KeyEvent;

public class PlayController {
    public Rect rect;
    public KL keyListenner = null;

    public PlayController(Rect rect, KL keyListenner) {
        this.rect = rect;
        this.keyListenner = keyListenner;
    }

    public PlayController(Rect rect) {
        this.rect = rect;
        this.keyListenner = null;
    }

    public void update(double dt) {
        if(keyListenner != null) {
            if (keyListenner.isKeyPressed(KeyEvent.VK_DOWN)) {
                moveDown(dt);
            } else if (keyListenner.isKeyPressed(KeyEvent.VK_UP)) {
                moveUp(dt);
            }
        } else {

        }
    }

    public void moveUp(double dt) {
        if (rect.y - Constants.PADDLE_SPEED * dt > Constants.TOOLBAR_HEIGHT) {
            this.rect.y -= Constants.PADDLE_SPEED * dt;
        }
    }

    public void moveDown(double dt) {
        if ((rect.y + Constants.PADDLE_SPEED * dt) + rect.h < Constants.SCREEN_HEIGHT - Constants.INSETS_BOTTOM) {
            this.rect.y += Constants.PADDLE_SPEED * dt;
        }
    }
}
