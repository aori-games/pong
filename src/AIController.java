public class AIController {
    public PlayController playController;
    public Rect ball;

    public AIController(PlayController playController, Rect ball) {
        this.playController = playController;
        this.ball = ball;
    }

    public void update(double dt) {
        playController.update(dt);
        if(ball.y < playController.rect.y) {
            playController.moveUp(dt);
        } else if(ball.y + ball.h > playController.rect.y + playController.rect.h) {
            playController.moveDown(dt);
        }
    }
}
