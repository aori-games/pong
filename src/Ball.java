public class Ball {
    public Rect ball, leftPaddle, rightPaddle;

    private double vy = 10.0;
    private double vx = -200.0;
    private Text leftScoreText, rightScoreText;

    public Ball(Rect rect, Rect leftPaddle, Rect rightPaddle, Text leftScoreText, Text rightScoreText) {
        this.ball = rect;
        this.leftPaddle = leftPaddle;
        this.rightPaddle = rightPaddle;
        this.leftScoreText = leftScoreText;
        this.rightScoreText = rightScoreText;
    }

    public double calculateNewVelocityAngle(Rect paddle ) {
        double relativeIntersect = (paddle.y + (paddle.h / 2.0)) - (this.ball.y + (this.ball.h / 2.0));
        double normalIntersect = relativeIntersect / (paddle.h / 2.0);
        return Math.toRadians(normalIntersect * Constants.MAX_ANGLE);
    }

    public void update(double dt) {
        if (vx < 0.0) { //is going to the left
            if (
                    this.ball.x <= this.leftPaddle.x + this.leftPaddle.w
                            && this.ball.x + this.ball.w >= this.leftPaddle.x
                            && this.ball.y >= this.leftPaddle.y
                            && this.ball.y <= this.leftPaddle.y + this.leftPaddle.h
            ) {
                double theta = calculateNewVelocityAngle(leftPaddle);
                double newVx = (Math.cos(theta) * Constants.BALL_SPEED);
                double newVy = (-Math.sin(theta) * Constants.BALL_SPEED);

                double oldSign = Math.signum(vx);
                this.vx = newVx * (-1.0 * oldSign);
                this.vy = newVy;
            }
        } else if (vx >= 0) { //is going to the right
            if (
                    this.ball.x  + this.ball.w >= this.rightPaddle.x
                            && this.ball.x <= this.rightPaddle.x + this.rightPaddle.w
                            && this.ball.y >= this.rightPaddle.y
                            && this.ball.y <= this.rightPaddle.y + this.rightPaddle.h
            ) {
                double theta = calculateNewVelocityAngle(rightPaddle);
                double newVx = (Math.cos(theta) * Constants.BALL_SPEED);
                double newVy = (Math.sin(theta) * Constants.BALL_SPEED);

                double oldSign = Math.signum(vx);
                this.vx = newVx * (-1.0 * oldSign);
                this.vy = newVy;
            }
        }

        /* CHECK IF IT TOUCHES THE TOP */
        if(vy >= 0) {
            if (this.ball.y + this.ball.h > Constants.SCREEN_HEIGHT) {
                this.vy *= -1;
            }
        } else if (vy < 0) {
            if (this.ball.y < Constants.TOOLBAR_HEIGHT) {
                this.vy *= -1;
            }
        }

        this.ball.x += vx * dt;
        this.ball.y += vy * dt;

        if(this.ball.x + this.ball.w < leftPaddle.x) {
            int rightScore = Integer.parseInt(rightScoreText.text);
            rightScore++;
            rightScoreText.text = "" + rightScore;
            this.ball.x = (double) Constants.SCREEN_WIDTH / 2;
            this.ball.y = (double) Constants.SCREEN_HEIGHT / 2;
            this.vy = 10.0;
            this.vx = -150.0;
            if(rightScore == Constants.WIN_SCORE) {
                Main.changeState(2);
            }
        } else if (this.ball.x > rightPaddle.x + rightPaddle.w) {
            int leftScore = Integer.parseInt(leftScoreText.text);
            leftScore++;
            leftScoreText.text = "" + leftScore;
            this.ball.x = (double) Constants.SCREEN_WIDTH / 2;
            this.ball.y = (double) Constants.SCREEN_HEIGHT / 2;
            this.vy = 10.0;
            this.vx = -150.0;
            if(leftScore == Constants.WIN_SCORE) {
                Main.changeState(2);
            }
        }
    }
}
