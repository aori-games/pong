import javax.swing.*;
import java.awt.*;

public class Window extends JFrame implements Runnable {

    public Graphics2D graphic;
    public KL keyListener = new KL();
    public Rect player, ai, ball;
    public PlayController playerController;
    public AIController aiController;
    public Ball ballController;
    public Text leftScoreText, rightScoreText;
    private boolean isRunning = false;

    public Window() {
        this.setSize(Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
        this.setTitle(Constants.SCREEN_TITLE);
        this.setResizable(false);
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //make sure to actually close the project when close project
        this.setLocationRelativeTo(null);
        this.addKeyListener(keyListener);
        this.isRunning = true;

        Constants.TOOLBAR_HEIGHT = this.getInsets().top;
        Constants.INSETS_BOTTOM = this.getInsets().bottom;

        player = new Rect(Constants.HORRZONTAL_PADDING, ((double) Constants.SCREEN_HEIGHT / 2 - Constants.PADDLE_HEIGHT / 2), Constants.PADDLE_WIDTH, Constants.PADDLE_HEIGHT, Constants.PADDLE_COLOR);
        ball = new Rect((double) Constants.SCREEN_WIDTH / 2 - (Constants.BALL_WIDTH / 2), (double) Constants.SCREEN_HEIGHT / 2, Constants.BALL_WIDTH, Constants.BALL_HEIGHT, Constants.BALL_COLOR);
        ai = new Rect(Constants.SCREEN_WIDTH - Constants.PADDLE_WIDTH - Constants.HORRZONTAL_PADDING, ((double) Constants.SCREEN_HEIGHT / 2 - Constants.PADDLE_HEIGHT / 2), Constants.PADDLE_WIDTH, Constants.PADDLE_HEIGHT, Constants.PADDLE_COLOR);
        leftScoreText = new Text(0,new Font("Times New Roman", Font.PLAIN,Constants.TEXT_SIZE),Constants.TEXT_X_POS,Constants.TEXT_Y_POS);
        rightScoreText = new Text(0,new Font("Times New Roman", Font.PLAIN,Constants.TEXT_SIZE),(Constants.SCREEN_WIDTH - Constants.TEXT_X_POS - 16),Constants.TEXT_Y_POS);

        playerController = new PlayController(player, keyListener);
        aiController = new AIController(new PlayController(ai),ball);
        ballController = new Ball(ball,player,ai,leftScoreText,rightScoreText);
        graphic = (Graphics2D) this.getGraphics();
    }

    public void update(double dt) {
        Image dbImage = createImage(getWidth(), getHeight());
        Graphics dbGraphics = dbImage.getGraphics();
        this.draw(dbGraphics);
        graphic.drawImage(dbImage, 0, 0, this);

        playerController.update(dt);
        ballController.update(dt);
        aiController.update(dt);
    }

    public void draw(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(Color.BLACK);
        g2d.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);

        player.draw(g2d);
        ai.draw(g2d);
        ball.draw(g2d);
        leftScoreText.draw(g2d);
        rightScoreText.draw(g2d);
    }

    public void stop() {
        isRunning = false;
    }

    @Override
    public void run() {
        double lastFrameTime = 0.0;
        while (isRunning) {
            double time = Time.getTime(); //get current time
            double deltaTime = time - lastFrameTime; //get the difference of time pasted since the last frame
            lastFrameTime = time;
            update(deltaTime);
        }
        this.dispose();
    }
}
