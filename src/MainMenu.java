import javax.swing.*;
import java.awt.*;

public class MainMenu extends JFrame implements Runnable {

    public Graphics2D graphic;
    public KL keyListener = new KL();
    public Text startGame, exitGame, pong;
    private final ML mouseListener = new ML();
    public boolean isRunning = true;

    public MainMenu() {
        this.setSize(Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
        this.setTitle(Constants.SCREEN_TITLE);
        this.setResizable(false);
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //make sure to actually close the project when close project
        this.setLocationRelativeTo(null);
        this.addKeyListener(keyListener);
        this.addMouseListener(mouseListener);
        this.addMouseMotionListener(mouseListener);
        this.startGame = new Text("Start Game", new Font("Times New Roman", Font.PLAIN, 40), Constants.SCREEN_WIDTH / 2.0 - 90, Constants.SCREEN_HEIGHT / 2.0, Color.WHITE);
        this.exitGame = new Text("Exit Game", new Font("Times New Roman", Font.PLAIN, 40), Constants.SCREEN_WIDTH / 2.0 - 80.0, Constants.SCREEN_HEIGHT / 2.0 + 60, Color.WHITE);
        this.pong = new Text("Pong", new Font("Times New Roman", Font.PLAIN, 80), Constants.SCREEN_WIDTH / 2.0 - 80.0, Constants.SCREEN_HEIGHT / 2.0 - 100, Color.WHITE);
        graphic = (Graphics2D) getGraphics();
    }

    public void update(double dt) {
        Image dbImage = createImage(getWidth(), getHeight());
        Graphics dbGraphics = dbImage.getGraphics();
        this.draw(dbGraphics);
        graphic.drawImage(dbImage, 0, 0, this);
    }

    public void draw(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(Color.BLACK);
        g2d.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
        this.startGame.draw(g2d);
        this.exitGame.draw(g2d);
        this.pong.draw(g2d);

        if (mouseListener.getMouseX() > startGame.x && mouseListener.getMouseX() < startGame.x + startGame.width &&
                mouseListener.getMouseY() > startGame.y - startGame.height / 2 &&
                mouseListener.getMouseY() < startGame.y + startGame.height + startGame.height / 2) {
            startGame.color = new Color(255, 255, 255, 166);
            if(mouseListener.isPressed()) {
                Main.changeState(1);
            }
        } else {
            startGame.color = Color.WHITE;
        }
        if (mouseListener.getMouseX() > exitGame.x && mouseListener.getMouseX() < exitGame.x + exitGame.width &&
                mouseListener.getMouseY() > exitGame.y - exitGame.height / 2 &&
                mouseListener.getMouseY() < exitGame.y + exitGame.height + exitGame.height / 2) {
            exitGame.color = new Color(255, 255, 255, 166);
            if(mouseListener.isPressed()) {
                Main.changeState(2);
            }
        } else {
            exitGame.color = Color.WHITE;
        }
    }

    public void stop() {
        isRunning = false;
    }

    @Override
    public void run() {
        double lastFrameTime = 0.0;
        while (isRunning) {
            double time = Time.getTime(); //get current time
            double deltaTime = time - lastFrameTime; //get the difference of time pasted since the last frame
            lastFrameTime = time;
            update(deltaTime);
        }
        this.dispose();
    }
}
